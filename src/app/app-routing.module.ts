import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const appRoutes: Routes = [
  { path: '', redirectTo: 'inversion-publica', pathMatch: 'full' },
  { path: 'inversion-publica', loadChildren: () => import('./map/map.module').then(m => m.MapModule) }
];
@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
