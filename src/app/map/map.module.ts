import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapRoutingModule } from './map-routing.module';
import { InversionPublicaComponent } from './containers/inversion-publica/inversion-publica.component';
import { HondurasRegionService } from './services/honduras-region.service';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { ShareModule } from '../share/share.module';
import {CabinetService} from './services/cabinet.service';
import {SpecificSourceService} from './services/specific-source.service';
import {InstitutionService} from './services/institution.service';
import {SectorService} from './services/sector.service';
import {ProjectService} from './services/project.service';
import {ComponentService} from './services/component.service';
import {FileService} from './services/file.service';



@NgModule({
  declarations: [InversionPublicaComponent],
  imports: [
    CommonModule,
    MapRoutingModule,
    LeafletModule,
    ShareModule
  ],
  providers: [
    CabinetService,
    SpecificSourceService,
    InstitutionService,
    SectorService,
    ProjectService,
    ProjectService,
    ComponentService,
    HondurasRegionService,
    FileService
  ]
})
export class MapModule { }
