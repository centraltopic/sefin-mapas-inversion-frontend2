import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {TemplateService} from '../../core/services/template.service';
import {BackendObject} from '../../core/models/model';
import {tap} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {Observable, of} from 'rxjs';

const URL = 'componente';

@Injectable()
export class ComponentService extends TemplateService {

  constructor(protected readonly http: HttpClient) {
    super(http, URL);
  }

  searchByComponentId(id: string): Observable<any> {
    if (id === null) {
      return of([]);
    }
    return this.http.get<BackendObject[]>(`${environment.baseUrl}/${URL}/?id_componente=${id}`).pipe(
      tap(_ => console.log(`searchBySeparata matching "${id}"`))
    );
  }
}
