import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable, of} from 'rxjs';

@Injectable()
export class HondurasRegionService {

  constructor(protected readonly http: HttpClient) { }

  getAllRegions(): Observable<any> {
    return this.http.get('/assets/geojson/hn-all.geo.json');
  }

  getHot(): Observable<any> {
    return this.http.get('/assets/geojson/hn-all-heat.geo.json');
  }

  getAllDeparments(): Observable<any> {
    return this.http.get('/assets/geojson/deparments_hn.geo.json');
  }

  getRegion1(): Observable<any> {
    return this.http.get('/assets/geojson/hn-region1.geo.json');
  }

  getRegion2(): Observable<any> {
    return this.http.get('/assets/geojson/hn-region2.geo.json');
  }

  getRegion3(): Observable<any> {
    return this.http.get('/assets/geojson/hn-region3.geo.json');
  }

  getRegion4(): Observable<any> {
    return this.http.get('/assets/geojson/hn-region4.geo.json');
  }

  getRegion5(): Observable<any> {
    return this.http.get('/assets/geojson/hn-region5.geo.json');
  }

  getRegion6(): Observable<any> {
    return this.http.get('/assets/geojson/hn-region6.geo.json');
  }

  getRegion7(): Observable<any> {
    return this.http.get('/assets/geojson/hn-region7.geo.json');
  }

  getMunicipality1(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_01.geo.json');
  }

  getMunicipality2(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_02.geo.json');
  }

  getMunicipality3(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_03.geo.json');
  }

  getMunicipality4(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_04.geo.json');
  }

  getMunicipality5(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_05.geo.json');
  }

  getMunicipality6(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_06.geo.json');
  }

  getMunicipality7(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_07.geo.json');
  }

  getMunicipality8(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_08.geo.json');
  }

  getMunicipality9(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_09.geo.json');
  }

  getMunicipality10(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_10.geo.json');
  }

  getMunicipality11(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_11.geo.json');
  }

  getMunicipality12(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_12.geo.json');
  }

  getMunicipality13(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_13.geo.json');
  }

  getMunicipality14(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_14.geo.json');
  }

  getMunicipality15(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_15.geo.json');
  }

  getMunicipality16(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_16.geo.json');
  }

  getMunicipality17(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_17.geo.json');
  }

  getMunicipality18(): Observable<any> {
    return this.http.get('/assets/geojson/municipios_18.geo.json');
  }

  getAllProjects(): Observable<any> {
    return this.http.get('/assets/geojson/hn-all-projects.geo.json');
  }
  getFewProjects(): Observable<any> {
    return this.http.get('/assets/geojson/hn-all-projects-few.geo.json');
  }
}
