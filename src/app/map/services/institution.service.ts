import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {TemplateService} from '../../core/services/template.service';

const URL = 'instituciones';

@Injectable()
export class InstitutionService extends TemplateService {

  constructor(protected readonly http: HttpClient) {
    super(http, URL);
  }
}
