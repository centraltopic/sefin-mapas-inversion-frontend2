import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {TemplateService} from '../../core/services/template.service';

const URL = 'fuente_especifica';

@Injectable()
export class SpecificSourceService extends TemplateService {

  constructor(protected readonly http: HttpClient) {
    super(http, URL);
  }
}
