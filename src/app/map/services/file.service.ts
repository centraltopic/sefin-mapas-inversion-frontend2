import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {TemplateService} from '../../core/services/template.service';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {tap} from 'rxjs/operators';

const URL = 'download';

@Injectable()
export class FileService extends TemplateService {

  constructor(protected readonly http: HttpClient) {
    super(http, URL);
  }

  getCatalogFile() {
    return this.getOfURL(`${URL}?open_data=SECTORES,DEPARTAMENTOS,MUNICIPIOS,INSTITUCION,GABINETE,FUENTE_ESPECIFICA,CATEGORIAS_INVERSION`,
      {responseType: 'text'});
  }

  searchByFilterProjectFile(cabinet: string, institution: string, sector: string, specificSource: string ): Observable<any> {
    let url = `${environment.baseUrl}/${URL}/?open_data=MATRIZ_COMPONENTES_PROYECTO&`;

    let noFilter = true;
    if (cabinet !== '') {
      noFilter = false;
      url = url + `gabinete=${cabinet}`;
    }

    if (institution !== '') {
      if (noFilter) {
        url = url + `institucion=${institution}`;
      } else {
        url = url + `&institucion=${institution}`;
      }
      noFilter = false;
    }

    if (sector !== '') {
      if (noFilter) {
        url = url + `sector=${sector}`;
      } else {
        url = url + `&sector=${sector}`;
      }
      noFilter = false;
    }

    if (specificSource !== '') {
      if (noFilter) {
        url = url + `fuente_especifica=${specificSource}`;
      } else {
        url = url + `&fuente_especifica=${specificSource}`;
      }
      noFilter = false;
    }

    if (noFilter) {
      url = url + `gabinete=0`;
    }
    return this.http.get(url, {responseType: 'text'}).pipe(
      tap(_ => console.log(`searchBySeparata matching gabinete="${cabinet}"
        institution="${institution}" sector="${sector}" specificSource="${specificSource}"`))
    );
  }

  searchByFilterCatalogAndProjectFile(cabinet: string, institution: string, sector: string, specificSource: string ): Observable<any> {
    let url = `${environment.baseUrl}/${URL}/?open_data=SECTORES,DEPARTAMENTOS,MUNICIPIOS,INSTITUCION,GABINETE,FUENTE_ESPECIFICA,CATEGORIAS_INVERSION,MATRIZ_COMPONENTES_PROYECTO&`;
    let noFilter = true;
    if (cabinet !== '') {
      noFilter = false;
      url = url + `gabinete=${cabinet}`;
    }

    if (institution !== '') {
      if (noFilter) {
        url = url + `institucion=${institution}`;
      } else {
        url = url + `&institucion=${institution}`;
      }
      noFilter = false;
    }

    if (sector !== '') {
      if (noFilter) {
        url = url + `sector=${sector}`;
      } else {
        url = url + `&sector=${sector}`;
      }
      noFilter = false;
    }

    if (specificSource !== '') {
      if (noFilter) {
        url = url + `fuente_especifica=${specificSource}`;
      } else {
        url = url + `&fuente_especifica=${specificSource}`;
      }
      noFilter = false;
    }

    if (noFilter) {
      url = url + `gabinete=0`;
    }
    return this.http.get(url, {responseType: 'text'}).pipe(
      tap(_ => console.log(`searchBySeparata matching gabinete="${cabinet}"
        institution="${institution}" sector="${sector}" specificSource="${specificSource}"`))
    );
  }

  public dataURItoBlob(dataURI: string) {

    // convert base64/URLEncoded data component to raw binary data held in a string
    let byteString = null;
    if ( dataURI.split(',')[0].indexOf('base64') >= 0) {
      byteString = atob(dataURI.split(',')[1]);
    } else {
      byteString = unescape(dataURI.split(',')[1]);
    }
    // separate out the mime component
    const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    // write the bytes of the string to a typed array
    const  ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], {type: mimeString});
  }

  public getBase64( dataFile: File): FileReader {
    const reader = new FileReader();
    reader.onloadend = function() {};
    reader.readAsDataURL(dataFile);
    return reader;
  }


}
