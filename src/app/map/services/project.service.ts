import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {TemplateService} from '../../core/services/template.service';
import {Observable, of} from 'rxjs';
import {tap} from 'rxjs/operators';
import {environment} from '../../../environments/environment';

const URL = 'proyectos';

@Injectable()
export class ProjectService extends TemplateService {

  constructor(protected readonly http: HttpClient) {
    super(http, URL);
  }

  searchByFilter(cabinet: string, institution: string, sector: string, specificSource: string ): Observable<any> {
    let url = `${environment.baseUrl}/${URL}/?`;
    let noFilter = true;
    if (cabinet !== '') {
      noFilter = false;
      url = url + `gabinete=${cabinet}`;
    }

    if (institution !== '') {
      if (noFilter) {
        url = url + `institucion=${institution}`;
      } else {
        url = url + `&institucion=${institution}`;
      }
      noFilter = false;
    }

    if (sector !== '') {
      if (noFilter) {
        url = url + `sector=${sector}`;
      } else {
        url = url + `&sector=${sector}`;
      }
      noFilter = false;
    }

    if (specificSource !== '') {
      if (noFilter) {
        url = url + `fuente_especifica=${specificSource}`;
      } else {
        url = url + `&fuente_especifica=${specificSource}`;
      }
      noFilter = false;
    }

    if (noFilter) {
      url = url + `gabinete=0`;
    }
    return this.http.get(url).pipe(
     tap(_ => console.log(`searchBySeparata matching gabinete="${cabinet}"
        institution="${institution}" sector="${sector}" specificSource="${specificSource}"`))
   );
  }
}
