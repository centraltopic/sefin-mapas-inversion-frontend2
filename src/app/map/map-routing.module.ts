import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InversionPublicaComponent } from './containers/inversion-publica/inversion-publica.component';


const routes: Routes = [{ path: '', component: InversionPublicaComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapRoutingModule { }
