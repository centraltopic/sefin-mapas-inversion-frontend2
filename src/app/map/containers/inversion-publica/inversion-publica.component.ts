import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { HondurasRegionService } from '../../services/honduras-region.service';
import { icon, latLng, latLngBounds, Map, geoJSON, Layer, layerGroup, marker, point, polyline,
  tileLayer, Browser, Control, DomUtil, LatLngBounds , Evented, Icon, Util, LayerGroup, DomEvent, Marker, Popup} from 'leaflet';
import {CabinetService} from '../../services/cabinet.service';
import {SpecificSourceService} from '../../services/specific-source.service';
import {InstitutionService} from '../../services/institution.service';
import {SectorService} from '../../services/sector.service';
import {ProjectService} from '../../services/project.service';
import {NgProgress, NgProgressRef} from '@ngx-progressbar/core';
import * as L from 'leaflet';
import '../../../../../node_modules/leaflet-extra-markers/dist/js/leaflet.extra-markers.js';
import {ComponentService} from '../../services/component.service';
import {FileService} from '../../services/file.service';
import * as FileSaver from 'file-saver';
import swal from 'sweetalert2';

@Component({
  selector: 'app-inversion-publica',
  templateUrl: './inversion-publica.component.html',
  styleUrls: ['./inversion-publica.component.css']
})
export class InversionPublicaComponent implements OnInit {
    public isRegion = true;
    public hondurasRegionsData: any;
    public hondurasHotData: any;
    public hondurasDeparmentsData: any;
    public hondurasSelectedRegionData: any;
    public hondurasSelecteProjectData: any;
    public hondurasSelectedMunicipalityData: any;

    public allRegionsGeojson: any;
    public hotGeojson: any;
    public allDeparmentsGeojson: any;

    public selectedRegionsGeojson: any;
    public selectedMunicipalityGeojson: any;
    public onEachFeatureMapFuntion: any;
    public onEachFeatureHotFuntion: any;
    public onEachFeatureMapDeparmentFuntion: any;
    public onEachFeatureRegionFuntion: any;
    public onEachMunicipalityDeparmentFuntion: any;

    public styleMap: any;
    public styleHot: any;
    public styleMapDeparments: any;

    public getColor: any;
    public getColorDeparments: any;
    public resetButton: any;
    public catalogButton: any;
    public projectButton: any;
    public catalogAndProjectButton: any;
    public filterButton: any;

    public info: any;
    public legend: any;
    public legendHot: any;
    public investmentCategory: any;

    public cabinetList: any;
    public cabinetCheckBoxList: any;
    public specificSourceList: any;
    public specificSourceCheckBoxList: any;
    public institutionList: any;
    public institutionCheckBoxList: any;
    public sectorList: any;
    public sectorCheckBoxList: any;
    public investmentCategoryCheckBoxList: any;

    public cacheCabinetFilter: string;
    public cacheSpecificSourceFilter: string;
    public cacheInstitutionFilter: string;
    public cacheSectorFilter: string;

    public capitalizableGoodIcon: any;
    public goodIcon: any;
    public workIcon: any;
    public otherIcon: any;
    public administrativeExpenseIcon: any;
    public serviceIcon: any;
    public materialIcon: any;
    public personalServiceIcon: any;
    public noPersonalServiceIcon: any;

    public selectedCapitalizableGoodGeojson: any;
    public selectedGoodGeojson: any;
    public selectedWorkGeojson: any;
    public selectedOtherGeojson: any;
    public selectedAdministrativeExpenseGeojson: any;
    public selectedServiceGeojson: any;
    public selectedMaterialGeojson: any;
    public selectedPersonalGeojson: any;
    public selectedNoPersonalGeojson: any;

    public progressRef: NgProgressRef;

    // Define our base layers so we can reference them multiple times
    public streetMaps = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    public wMaps = tileLayer('http://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
      detectRetina: true,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    public options = {
      layers: [],
      zoom: 7,
      center: latLng([ 46.879966, -121.726909 ])
    };

    public layers: Layer[] = [];

    public layersControl = {
      baseLayers: {
      },
      overlays: {
      }
    };

    public fitBounds: LatLngBounds;

    constructor(
      private readonly hondurasRegionService: HondurasRegionService,
      private readonly cabinetService: CabinetService,
      private readonly specificSourceService: SpecificSourceService,
      private readonly institutionService: InstitutionService,
      private readonly sectorService: SectorService,
      private readonly projectService: ProjectService,
      private readonly fileService: FileService,
      private readonly componentService: ComponentService,
      private progress: NgProgress,
      private changeDetector: ChangeDetectorRef) {
    }

    ngOnInit(): void {
      this.progressRef = this.progress.ref('myProgress');
      this.setStylesAndColor();
      this.setCustomButtons();
      this.setCustomIcons();
      this.setInfoWindow();
      this.setLegendWindow();
      this.setLegendHotWindow();
      this.setCheckboxList();
      this.setFunctionConfiguration();
      this.hondurasRegionService.getAllRegions().subscribe(data => {
        this.hondurasRegionsData = data;
        this.hondurasRegionService.getHot().subscribe(data2 => {
          this.hondurasHotData = data2;
          this.hondurasRegionService.getAllDeparments().subscribe(dat3 => {
            this.hondurasDeparmentsData = dat3;
            this.loadHondurasMap();
          });
        });
      });
    }

    private zoomToMapMethod() {
      const southWest = latLng(13, -83);
      const northEast = latLng(16.5, -90.7);
      const bounds = latLngBounds(southWest, northEast);
      this.fitBounds = bounds;

      if (this.selectedRegionsGeojson) {
        this.selectedRegionsGeojson.remove();
      }
      this.changeDetector.detectChanges();
    }

    private zoomToMapDeparmentMethod() {
      const southWest = latLng(13, -83);
      const northEast = latLng(16.5, -90.7);
      const bounds = latLngBounds(southWest, northEast);
      this.fitBounds = bounds;

      if (this.selectedMunicipalityGeojson) {
        this.selectedMunicipalityGeojson.remove();
      }
      this.changeDetector.detectChanges();
    }

    private convertChecktoListSelected(listName) {
      const listSelected = [];
      const parentList = document.getElementById(listName);
      const inputList = Array.prototype.slice.call(parentList.getElementsByClassName("checkmarkInput"));

      for (const input of inputList) {
        if (input.checked) {
          listSelected.push(input.id);
        }
      }
      return listSelected;
    }

    private filterProjects() {
      const sectorListSelected = this.convertChecktoListSelected('SECTOR');
      const institutionListSelected = this.convertChecktoListSelected('INSTITUCIÓN');
      const specificSourceListSelected = this.convertChecktoListSelected('FUENTE ESPECÍFICA');
      const cabinetListSelected = this.convertChecktoListSelected('GABÍNETE');

      console.log(sectorListSelected);

      console.log(institutionListSelected);

      console.log(specificSourceListSelected);

      console.log(cabinetListSelected);

      this.progressRef.start();
      if ( this.cacheCabinetFilter === cabinetListSelected.join() &&
          this.cacheInstitutionFilter === institutionListSelected.join() &&
          this.cacheSectorFilter === sectorListSelected.join() &&
          this.cacheSpecificSourceFilter === specificSourceListSelected.join()) {
          this.loadProject();
          this.progressRef.complete();
      } else {
        this.projectService.searchByFilter(cabinetListSelected.join(), institutionListSelected.join(),
          sectorListSelected.join(),  specificSourceListSelected.join()).subscribe(data => {
          this.cacheCabinetFilter = cabinetListSelected.join();
          this.cacheInstitutionFilter = institutionListSelected.join();
          this.cacheSectorFilter = sectorListSelected.join();
          this.cacheSpecificSourceFilter = specificSourceListSelected.join();
          this.hondurasSelecteProjectData = data;
          this.loadProject();
          this.progressRef.complete();
        });
      }
    }

    private getProjectInformation(e: any) {
      let row;
      let cell1;
      let cell2;
      let cellLenght = 0;
      this.componentService.searchByComponentId(e.target.feature.properties.ID).subscribe(data => {
        const content = L.DomUtil.create('div', 'rTable');

        if (data && data.rows && data.rows.length > 0) {
          Object.entries(data.rows[0]).forEach(entry => {

            switch (cellLenght) {
              case 0:
                row = DomUtil.create('table', 'rTableRow', content);
                cell1 = DomUtil.create('div', 'rTableCell', row);
                cell1.innerHTML = '<span><b>' + entry[0] + ':  </b>' + entry[1];
                cellLenght = 1;
                return '#846EA3';
                break;
              case 1:
                cell2 = DomUtil.create('div', 'rTableCell', row);
                cell2.innerHTML = '<span><b>' + entry[0] + ':  </b>' + entry[1];
                cellLenght = 0;
                return '#D05553';
                break;
            }
          });
        } else {
          content.innerHTML = '<h4> No se pudo Obtener la Infomración</h4>';
        }
        e.target.getPopup().setContent(content);
        e.target.getPopup().update();
      });
    }

    setStylesAndColor() {
      this.getColor = (name: string) => {
        switch (name) {
          case 'Region 1':
            return '#846EA3';
            break;
          case 'Region 2':
            return '#D05553';
            break;
          case 'Region 3':
            return '#3E9D3D';
            break;
          case 'Region 4':
            return '#79C0CB';
            break;
          case 'Region 5':
            return '#C74DC7';
            break;
          case 'Region 6':
            return '#FAFB68';
            break;
          case 'Region 7':
            return '#F6B577';
            break;
          case 'Lago de Yojoa':
            return '#0000FF';
            break;
          default:
            return '#FFFFFF';
            break;
        }
      }

      this.getColorDeparments = (name: string) => {
        switch (name) {
          case 'Atlantida':
            return '#846EA3';
            break;
          case 'Colon':
            return '#D05553';
            break;
          case 'Comayagua':
            return '#3E9D3D';
            break;
          case 'Copan':
            return '#79C0CB';
            break;
          case 'Cortes':
            return '#C74DC7';
            break;
          case 'Choluteca':
            return '#FAFB68';
            break;
          case 'El Paraiso':
            return '#080F0F';
            break;
          case 'Francisco Morazan':
            return '#1D8A99';
            break;
          case 'Gracias A Dios':
            return '#C04ABC';
            break;
          case 'Intibuca':
            return '#FFDBB5';
            break;
          case 'Islas de La Bahia':
            return '#E0A458';
            break;
          case 'La Paz':
            return '#419D78';
            break;
          case 'Lempira':
            return '#2D3047';
            break;
          case 'Ocotepeque':
            return '#C19875';
            break;
          case 'Olancho':
            return '#F2E3BC';
            break;
          case 'Santa Barbara':
            return '#414535';
            break;
          case 'Valle':
            return '#618985';
            break;
          case 'Yoro':
            return '#14FFF7';
            break;
          case 'Lago de Yojoa':
            return '#0000FF';
            break;
          default:
            return '#FFFFFF';
            break;
        }
      }

      this.styleMap = (feature: any) => {
        return {
          fillColor: this.getColor(feature.properties.NOMBRE),
          weight: 2,
          opacity: 1,
          color: 'white',
          dashArray: '3',
          fillOpacity: 0.5
        };
      };

      this.styleHot = (feature: any) => {
        return {
          fillColor: feature.properties.COLOR,
          weight: 2,
          opacity: 1,
          color: 'white',
          dashArray: '3',
          fillOpacity: 0.9
        };
      };

      this.styleMapDeparments = (feature: any) => {
        return {
          fillColor: this.getColorDeparments(feature.properties.NOMBRE),
          weight: 2,
          opacity: 1,
          color: 'white',
          dashArray: '3',
          fillOpacity: 0.5
        };
      };
    }

    setCustomButtons() {
        const customButtonControl = Control.extend({
          options: {
            position: 'topleft',
            inText: '&#8634;',
            inTitle: 'Vista inicial'
          },
          initialize: function(options) {
            Util.setOptions(this, options);
            this.position = this.options.position;
            this.inText = this.options.inText;
            this.inTitle = this.options.inTitle;
          },
          onAdd: function() {
            const container = DomUtil.create('div');
            this._createButton(
              this.options.inText, this.options.inTitle,
              'info',  container, this._click,  this);
            return container;
          },
          _click: (e) => {
            console.log('si me dio click');
          },
          _createButton: (html, title, className, container, fn, context) => {
            const link = DomUtil.create('a', className, container);
            link.innerHTML = html;
            link.title = title;
            const stop = DomEvent.stopPropagation;
            DomEvent
              .on(link, 'click', stop)
              .on(link, 'mousedown', stop)
              .on(link, 'dblclick', stop)
              .on(link, 'click', DomEvent.preventDefault)
              .on(link, 'click', fn, context);
            return link;
          }
        });
        this.resetButton = new customButtonControl();
        this.resetButton.initialize({  position: 'topleft', inText: '&#8634; Vista inicial', inTitle: 'Vista inicial'});
        this.resetButton._click = (e: any) => {
         this.zoomToMapMethod();
         this.zoomToMapDeparmentMethod();
        };

        this.catalogButton = new customButtonControl();
        this.catalogButton.initialize({  position: 'topleft', inText: '<i class="fas fa-cloud-download-alt"></i>  Catálogos', inTitle: 'Catálogos'});
        this.catalogButton._click = (e: any) => {
          this.getCatalogFile();
        };

        this.projectButton = new customButtonControl();
        this.projectButton.initialize({  position: 'topleft', inText: '<i class="fas fa-cloud-download-alt"></i> Componentes', inTitle: 'Componentes'});
        this.projectButton._click = (e: any) => {
          this.getProjectFile();
        };

        this.catalogAndProjectButton = new customButtonControl();
        this.catalogAndProjectButton.initialize({  position: 'topleft', inText: '<i class="fas fa-cloud-download-alt"></i> Catálogos y Componentes', inTitle: 'Catálogos y Componentes'});
        this.catalogAndProjectButton._click = (e: any) => {
          this.getCatalogAndProjectFile();
        };

        this.filterButton = new customButtonControl();
        this.filterButton.initialize({  position: 'bottomleft', inText: '&#128269; Aplicar Filtros', inTitle: 'Aplicar Filtros'});
        this.filterButton._click = (e: any) => {
          this.filterProjects();
        };
    }

  setCustomIcons() {
      this.capitalizableGoodIcon = L.ExtraMarkers.icon({
        icon: 'fa-hospital',
        markerColor: 'red',
        shape: 'square',
        prefix: 'far'
      });
      this.goodIcon = L.ExtraMarkers.icon({
        icon: 'fa-hospital',
        markerColor: 'blue',
        shape: 'square',
        prefix: 'fas'
      });
      this.workIcon = L.ExtraMarkers.icon({
        icon: 'fa-bacon',
        iconColor: 'black',
        markerColor: 'orange-dark',
        shape: 'square',
        prefix: 'fas'
      });
      this.otherIcon = L.ExtraMarkers.icon({
        icon: 'fa-landmark',
        markerColor: 'yellow',
        shape: 'square',
        prefix: 'fas'
      });
      this.administrativeExpenseIcon = L.ExtraMarkers.icon({
        icon: 'fa-file-invoice-dollar',
        markerColor: 'blue-dark',
        shape: 'square',
        prefix: 'fas'
      });
      this.serviceIcon = L.ExtraMarkers.icon({
        icon: 'fa-user-md',
        markerColor: 'orange',
        shape: 'square',
        prefix: 'fas'
      });
      this.materialIcon = L.ExtraMarkers.icon({
        icon: 'fa-cash-register',
        markerColor: 'cyan',
        shape: 'square',
        prefix: 'fas'
      });
      this.personalServiceIcon = L.ExtraMarkers.icon({
        icon: 'fa-user-tie',
        markerColor: 'purple',
        shape: 'square',
        prefix: 'fas'
      });
      this.noPersonalServiceIcon = L.ExtraMarkers.icon({
        icon: 'fa-user-slash',
        markerColor: 'violet',
        shape: 'square',
        prefix: 'fas'
      });
    }

    setInfoWindow() {
      this.info = new Control();
      let divInfo: any;
      this.info.onAdd = () => {
        divInfo = DomUtil.create('div', 'info'); // create a div with a class "info"
        this.info.update();
        return divInfo;
      };
      this.info.update = (type: string, properties: any) => {
        switch (type) {
          case 'general':
            divInfo.innerHTML = (properties.NOMBRE ?
              '<h4>' + properties.NOMBRE + '</h4>'
              : '' ) +
              (properties.TOTAL_PROYECTOS ?
                '<b> Total de Proyectos: ' + properties.TOTAL_PROYECTOS + '</b><br />'
                : '<b> Total de Proyectos: 0 </b><br />') +
              (properties.MONTO_GLOBAL ?
                '<b> Monto Global: ' + properties.MONTO_GLOBAL + '</b><br />'
                : '<b> Monto Global: 0M</b><br />') +
              (properties.TOTAL_COMPONENTES ?
                '<b> Total de Componentes: ' + properties.TOTAL_COMPONENTES + '</b><br />'
                : '<b> Total de Componentes: 0</b><br />');
            break;
          case 'hot':
            divInfo.innerHTML = (properties.NOMBRE ?
              '<h4>' + properties.NOMBRE + '</h4>'
              : '' ) +
              (properties.TOTAL_PROYECTOS ?
                '<b> Total de Proyectos: ' + properties.TOTAL_PROYECTOS + '</b><br />'
                : '') +
              (properties.MONTO_INVERSION ?
                '<b> Monto Inversión: ' + properties.MONTO_INVERSION + '</b><br />'
                : '') +
              (properties.TOTAL_COMPONENTES ?
                '<b> Total de Componentes: ' + properties.TOTAL_COMPONENTES + '</b><br />'
                : '');
            break;
          default:
            break;
        }
      };
    }

    setLegendWindow() {
      this.legend = new Control({position: 'bottomright'});
      this.legend.onAdd = () => {
        const div = DomUtil.create('div', 'info legend');
        const grades = ['Region 1', 'Region 2', 'Region 3', 'Region 4', 'Region 5', 'Region 6', 'Region 7'];
        const labels = ['Valle de Sula', 'Occidente y Lempa', 'Lean y Aguán', 'Golfo de Fonseca', 'Valle de Comayagua',
        'Centro y El Paraíso', 'Norte y Valles de Olancho'];
        const letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];
        for (let i = 0; i < grades.length; i++) {
          div.innerHTML +=
            '<i style="background:' + this.getColor(grades[i]) + '">' + letters[i] + '</i> ' +
            labels[i] + '<br>' ;
        }
        return div;
      };
    }

    setLegendHotWindow() {
      this.legendHot = new Control({position: 'bottomright'});
      this.legendHot.onAdd = () => {
        const div = DomUtil.create('div', 'info legend');
        const color = ['#FCFAAB', '#FAE29B', '#F8CA8B', '#F7B37B', '#F59B6C', '#F4845C', '#F26C4C', '#F0543D', '#EF3D2D', '#ED251D', '#EC0E0E'];
        const labels = ['0M - 0.5M', '0.5 - 1M', '1M - 3M', '3M - 5M', '5M - 7M', '7M - 15M', '15M - 50M', '50M - 100M', '100M - 500M', '500M - 1000M', '1000M - o más'];
        const numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'];
        for (let i = 0; i < numbers.length; i++) {
          div.innerHTML +=
            '<i style="background:' + (color[i]) + '">' + numbers[i] + '</i> ' +
            labels[i] + '<br>' ;
        }
        return div;
      };
    }

    setInvestmentCategoryWindow() {
      this.investmentCategory = new Control({position: 'topleft'});
      this.investmentCategory.onAdd = () => {
        const div = DomUtil.create('div', 'info legend');
        const colors = ['#991F23', '#106AB6', '#D5351E', '#F4B322', '#184157', '#EE8A19', '#2EA7DC', '#512C62', '#891E87'];
        const labels = ['BIENES CAPITALIZABLES', 'BIENES', 'OBRAS', 'OTROS', 'GASTOS ADMINISTRATIVOS ',
          'SERVICIOS', 'MATERIALES Y SUMINISTROS', 'SERVICIOS PERSONALES', 'SERVICIOS NO PERSONALES'];
        const icons = ['far fa-hospital', 'fas fa-hospital', 'fas fa-bacon', 'fas fa-landmark', 'fas fa-file-invoice-dollar', 'fas fa-user-md',
          'fas fa-cash-register', 'fas fa-user-tie', 'fas fa-user-slash'];
        for (let i = 0; i < colors.length; i++) {
          div.innerHTML +=
            '<i class ="' + icons[i] + '"style="text-align: center; color: white; background:' + colors[i] + '"></i> ' +
            labels[i] + '<br>' ;
        }
        return div;
      };
    }

    setCheckboxList() {
      const CheckboxList = Control.extend({
        options: {
          _title: 'Título',
          _menuClass: 'menu',
          _items: [],
          itemArrow: '&#10148;',	//visit: http://character-code.com/arrows-html-codes.php
          _position: 'bottomleft',
          _update: false,
          _custom: false,
          _checked: false
        },
        initialize: function(options) {
          Util.setOptions(this, options);
          this._title = this.options.title;
          this._menuClass = this.options.menuClass;
          this._items = this.options.items;
          this._position = this.options.position;
          this._update = this.options.update;
          this._custom = this.options.custom;
          this._checked = this.options.checked;
        },

        onAdd: function(map: Map) {
          const container = DomUtil.create('div', 'list-markers');
          container.setAttribute('id', this._title);
          container.innerHTML = '<input type="checkbox" id="' + this._menuClass + '">';
          container.innerHTML +=  '<label for="' + this._menuClass + '">' + this._title + '</label>';

          const menu = DomUtil.create('div', 'menu-content', container);
          this._list = DomUtil.create('ul', 'list-markers-ul', menu);

          if (this._update) {
            this._updateList();
          }
          return container;
        },
        _createItem: function(item: any) {
          const li = DomUtil.create('li', 'list-markers-li');
          const a = DomUtil.create('a', '', li);
          const cbox = DomUtil.create('input', 'checkmarkInput', a );
          cbox.setAttribute('id', item.id);
          cbox.setAttribute('type', 'checkbox');
          if (this._checked) {
            cbox.setAttribute('checked', '');
          }

          const ctxt = DomUtil.create('span', 'checkmark', a);

          if (this._custom) {
            ctxt.setAttribute('style', 'font-size:12px; color:black; font-weight: bold;');
            ctxt.innerHTML = '<i class ="' + item.icon + '"style="text-align: center; color: white; background:' + item.background + '"></i> ' +
              item.nombre + '<br>' ;
          } else {
            ctxt.innerHTML = '<span style="color:black; font-weight: bold;">' + item.nombre + '</span> <b>';
          }
          return li;
        },
        _updateList: function() {
          const that = this;
          const n = 0;
          this._list.innerHTML = '';
          if (this._items) {
            this._items.forEach(item => {
              that._list.appendChild( that._createItem(item) );
            });
          }
        },
        _click: function () {
          console.log('si me dio click');
        },
      });

      this.cabinetCheckBoxList = new CheckboxList();
      this.cabinetCheckBoxList.initialize({items: [], title: 'GABÍNETE', menuClass: 'menu', position: 'bottomleft'});

      this.specificSourceCheckBoxList = new CheckboxList();
      this.specificSourceCheckBoxList.initialize({items: [], title: 'FUENTE ESPECÍFICA', menuClass: 'menu2', position: 'bottomleft'});

      this.institutionCheckBoxList = new CheckboxList();
      this.institutionCheckBoxList.initialize({items: [], title: 'INSTITUCIÓN', menuClass: 'menu3', position: 'bottomleft'});

      this.sectorCheckBoxList = new CheckboxList();
      this.sectorCheckBoxList.initialize({items: [], title: 'SECTOR', menuClass: 'menu4', position: 'bottomleft'});

      this.cabinetService.getList().subscribe(res => {
        if (res && res.rows && res.rows.length) {
          this.cabinetList = res.rows;
          this.cabinetCheckBoxList.initialize({items: this.cabinetList,
            title: 'GABÍNETE', menuClass: 'menu'});
          this.cabinetCheckBoxList._updateList();
        }
      });

      this.specificSourceService.getList().subscribe(res => {
        if (res && res.rows && res.rows.length) {
          this.specificSourceList = res.rows;
          this.specificSourceCheckBoxList.initialize({items: this.specificSourceList,
            title: 'FUENTE ESPECÍFICA', menuClass: 'menu2'});
          this.specificSourceCheckBoxList._updateList();
        }
      });

      this.institutionService.getList().subscribe(res => {
        if (res && res.rows && res.rows.length) {
          this.institutionList = res.rows;
          this.institutionCheckBoxList.initialize({items: this.institutionList,
            title: 'INSTITUCIÓN', menuClass: 'menu3'});
          this.institutionCheckBoxList._updateList();
        }
      });

      this.sectorService.getList().subscribe(res => {
        if (res && res.rows && res.rows.length) {
          this.sectorList = res.rows;
          this.sectorCheckBoxList.initialize({items: this.sectorList,
            title: 'SECTOR', menuClass: 'menu4'});
          this.sectorCheckBoxList._updateList();
        }
      });

      this.investmentCategoryCheckBoxList = new CheckboxList();
      this.investmentCategoryCheckBoxList.initialize({items:
          [{id: '1', nombre: 'BIENES CAPITALIZABLES', background: '#991F23', icon: 'far fa-hospital'},
           {id: '2', nombre: 'BIENES', background: '#106AB6', icon: 'fas fa-hospital'},
           {id: '3', nombre: 'OBRAS', background: '#D5351E', icon: 'fas fa-bacon'},
           {id: '4', nombre: 'OTROS', background: '#F4B322', icon: 'fas fa-landmark'},
           {id: '5', nombre: 'GASTOS ADMINISTRATIVOS', background: '#184157', icon: 'fas fa-file-invoice-dollar'},
           {id: '6', nombre: 'SERVICIOS', background: '#EE8A19', icon: 'fas fa-user-md'},
           {id: '7', nombre: 'MATERIALES Y SUMINISTROS', background: '#2EA7DC', icon: 'fas fa-cash-register'},
           {id: '8', nombre: 'SERVICIOS PERSONALES', background: '#512C62', icon: 'fas fa-user-tie'},
           {id: '9', nombre: 'SERVICIOS NO PERSONALES', background: '#891E87', icon: 'fas fa-user-slash'}],
        title: 'CATEGORIA INVERSIÓN', menuClass: 'menu5', position: 'bottomleft', update: true, custom: true, checked: true});
    }

    setFunctionConfiguration() {
      const highlightFeatureMapFuntion = (e: any) => {
        const layer = e.target;
        this.highlightFeature(layer);
        this.info.update('general', layer.feature.properties);
      };

      const highlightFeatureHotFuntion = (e: any) => {
        const layer = e.target;
        this.highlightFeature(layer);
        this.info.update('hot', layer.feature.properties);
      };

      const highlightFeatureMapDeparmentFuntion = (e: any) => {
        const layer = e.target;
        this.highlightFeature(layer);
        this.info.update('general', layer.feature.properties);
      };

      const resetHighlightMapFuntion = (e: any) => {
        this.allRegionsGeojson.resetStyle(e.target);
        this.info.update();
      };

      const resetHighlightRegionFuntion = (e: any) => {
        this.selectedRegionsGeojson.resetStyle(e.target);
        this.info.update();
      };

      const resetHighlightMunicipalityFuntion = (e: any) => {
        this.selectedMunicipalityGeojson.resetStyle(e.target);
        this.info.update();
      };

      const resetHighlightHotFuntion = (e: any) => {
        this.hotGeojson.resetStyle(e.target);
        this.info.update();
      };

      const resetHighlightMapDeparmentFuntion = (e: any) => {
        this.allDeparmentsGeojson.resetStyle(e.target);
        this.info.update();
      };

      const zoomToFeatureMap = (e: any) => {
        this.loadRegion(e.sourceTarget.feature.properties.NOMBRE);
        this.fitBounds = e.target.getBounds();
      };

      const zoomToMunicipalityMap = (e: any) => {
        this.loadMunicipality(e.sourceTarget.feature.properties.NOMBRE);
        this.fitBounds = e.target.getBounds();
      };

      const zoomToFeatureRegion = (e: any) => {
        this.fitBounds = e.target.getBounds();
        this.changeDetector.detectChanges();
      };

      const  zoomToMap = (e: any) => {
        this.zoomToMapMethod();
      };

      const  zoomToMapDeparment = (e: any) => {
        this.zoomToMapDeparmentMethod();
      };

      this.onEachFeatureMapFuntion = (feature: any, layer: any) => {
        layer.on({
          mouseover: highlightFeatureMapFuntion,
          mouseout: resetHighlightMapFuntion,
          click: zoomToFeatureMap
        });
      };

      this.onEachFeatureHotFuntion = (feature: any, layer: any) => {
        layer.on({
          mouseover: highlightFeatureHotFuntion,
          mouseout: resetHighlightHotFuntion,
          click: zoomToFeatureRegion,
          contextmenu: zoomToMap
        });
      };

      this.onEachFeatureMapDeparmentFuntion = (feature: any, layer: any) => {
        layer.on({
          mouseover: highlightFeatureMapDeparmentFuntion,
          mouseout: resetHighlightMapDeparmentFuntion,
          click: zoomToMunicipalityMap
        });
      };

      this.onEachFeatureRegionFuntion = (feature: any, layer: any) => {
        layer.on({
          mouseover: highlightFeatureMapFuntion,
          mouseout: resetHighlightRegionFuntion,
          click: zoomToFeatureRegion,
          contextmenu: zoomToMap
        });
      };

      this.onEachMunicipalityDeparmentFuntion = (feature: any, layer: any) => {
        layer.on({
          mouseover: highlightFeatureMapDeparmentFuntion,
          mouseout: resetHighlightMunicipalityFuntion,
          click: zoomToFeatureRegion,
          contextmenu: zoomToMapDeparment
        });
      };
    }

    loadHondurasMap() {
      this.allRegionsGeojson = geoJSON(this.hondurasRegionsData,
        { style: this.styleMap, onEachFeature: this.onEachFeatureMapFuntion});
      this.hotGeojson = geoJSON(this.hondurasHotData,
        { style: this.styleHot, onEachFeature: this.onEachFeatureHotFuntion});

      this.allDeparmentsGeojson = geoJSON(this.hondurasDeparmentsData,
        { style: this.styleMapDeparments, onEachFeature: this.onEachFeatureMapDeparmentFuntion});


      const normalRegion = layerGroup([this.streetMaps, this.allRegionsGeojson]);
      const citiesRegion = layerGroup([this.streetMaps, this.allDeparmentsGeojson]);

      this.layersControl.baseLayers['Regiones'] = normalRegion;
      this.layersControl.baseLayers['Departamentos'] = citiesRegion;
      this.layersControl.overlays['Calor'] = this.hotGeojson;
      this.layers.push(normalRegion);
    }

    onMapReady(map: Map) {
      const iconRetinaUrl = 'assets/marker-icon-2x.png';
      const iconUrl = 'assets/marker-icon.png';
      const shadowUrl = 'assets/marker-shadow.png';
      const iconDefault = icon({
      iconRetinaUrl,
      iconUrl,
      shadowUrl,
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      tooltipAnchor: [16, -28],
      shadowSize: [41, 41]
      });
      Marker.prototype.options.icon = iconDefault;


      const southWest = latLng(13, -83);
      const northEast = latLng(16.5, -90.7);
      const bounds = latLngBounds(southWest, northEast);

      map.fitBounds(bounds);
      map.setMaxZoom(11);
      map.setMinZoom(8);
      map.setView([14.591640645706688,  -86.69783592224123], 8);
      map.zoomControl.remove();
      map.dragging.disable();
      map.doubleClickZoom.disable();
      map.scrollWheelZoom.disable();
      map.boxZoom.disable();
      map.keyboard.disable();


      map.addControl(this.info);
      map.addControl(this.legend);
      map.addControl(this.resetButton);
      map.addControl(this.catalogButton);
      map.addControl(this.projectButton);
      map.addControl(this.catalogAndProjectButton);


      map.addControl(this.investmentCategoryCheckBoxList);
      map.addControl(this.specificSourceCheckBoxList);
      map.addControl(this.institutionCheckBoxList);
      map.addControl(this.cabinetCheckBoxList);
      map.addControl(this.sectorCheckBoxList);
      map.addControl(this.filterButton);

      const layerAdd = (layerEvent: any) => {
        if (layerEvent.layer.feature) {
          if (layerEvent.layer.feature.properties.MONTO_INVERSION && layerEvent.layer.feature.properties.NOMBRE === 'Victoria') {
            if (this.isRegion) {
              map.removeControl(this.legend);
            }
            map.addControl(this.legendHot);
          }
        }
      };

      const layerRemove = (layerEvent: any) => {
        if (layerEvent.layer.feature) {
          if (layerEvent.layer.feature.properties.MONTO_INVERSION && layerEvent.layer.feature.properties.NOMBRE === 'Victoria') {
            map.removeControl(this.legendHot);
            if (this.isRegion) {
              map.addControl(this.legend);
            }
          }
        }
      };

      const baselayerchange = (layerEvent: any) => {
        if (this.selectedRegionsGeojson) {
          this.selectedRegionsGeojson.remove();
        }
        if (this.selectedMunicipalityGeojson) {
          this.selectedMunicipalityGeojson.remove();
        }

        if (this.isRegion) {
          this.isRegion = false;
          map.removeControl(this.legend);
          map.removeControl(this.legendHot);
        } else {
          this.isRegion = true;
          map.addControl(this.legend);
        }
      };

      map.on('layeradd', layerAdd);
      map.on('layerremove', layerRemove);
      map.on('baselayerchange', baselayerchange);



     /* this.popup = new Popup()
        .setLatLng({lat: 12, lng: 12})
        .setContent('<p>Hello world!<br />This is a nice popup.</p>').openOn(map);

      console.log(this.popup);*/

     /* L.marker([14.591640645706688,  -86.69783592224123], {icon: this.capitalizableGoodIcon}).addTo(map);

      L.marker([14.822640645706688,  -86.69883592224123], {icon: this.goodIcon}).addTo(map);

      L.marker([15.022640645706688,  -86.69883592224123], {icon: this.workIcon}).addTo(map);


      L.marker([15.222640645706688,  -86.69883592224123], {icon: this.otherIcon}).addTo(map);

      L.marker([15.422640645706688,  -86.69883592224123], {icon: this.administrativeExpenseIcon}).addTo(map);


      L.marker([15.622640645706688,  -86.69883592224123], {icon: this.serviceIcon}).addTo(map);


      L.marker([15.822640645706688,  -86.69883592224123], {icon: this.materialIcon}).addTo(map);


      L.marker([16.022640645706688,  -86.69883592224123], {icon: this.personalServiceIcon}).addTo(map);

      L.marker([16.222640645706688,  -86.69883592224123], {icon: this.noPersonalServiceIcon}).addTo(map);*/
    }

    highlightFeature(layer: any) {
      layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.7,
        fillColor: '#ffffff'
      });


      if (!Browser.ie && !Browser.opera && !Browser.edge) {
        layer.bringToFront();
      }
    }

    loadRegion(name: string) {
      switch (name) {
        case 'Region 1':
            this.hondurasRegionService.getRegion1().subscribe(data => {
              this.hondurasSelectedRegionData = data;
              this.loadMapRegion('Region 1');
            });
            break;
        case 'Region 2':
            this.hondurasRegionService.getRegion2().subscribe(data => {
              this.hondurasSelectedRegionData = data;
              this.loadMapRegion('Region 2');
            });
            break;
        case 'Region 3':
            this.hondurasRegionService.getRegion3().subscribe(data => {
              this.hondurasSelectedRegionData = data;
              this.loadMapRegion('Region 3');
            });
            break;
        case 'Region 4':
            this.hondurasRegionService.getRegion4().subscribe(data => {
              this.hondurasSelectedRegionData = data;
              this.loadMapRegion('Region 4');
            });
            break;
        case 'Region 5':
            this.hondurasRegionService.getRegion5().subscribe(data => {
              this.hondurasSelectedRegionData = data;
              this.loadMapRegion('Region 5');
            });
            break;
        case 'Region 6':
            this.hondurasRegionService.getRegion6().subscribe(data => {
              this.hondurasSelectedRegionData = data;
              this.loadMapRegion('Region 6');
            });
            break;
        case 'Region 7':
            this.hondurasRegionService.getRegion7().subscribe(data => {
              this.hondurasSelectedRegionData = data;
              this.loadMapRegion('Region 7');
            });
            break;
        case 'Lago de Yojoa':
          break;
        default:
          break;
      }
    }

    loadMunicipality(name: string) {
      switch (name) {
        case 'Atlantida':
          this.hondurasRegionService.getMunicipality1().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('Atlantida');
          });
          break;
        case 'Colon':
          this.hondurasRegionService.getMunicipality2().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('Colon');
          });
          break;
        case 'Comayagua':
          this.hondurasRegionService.getMunicipality3().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('Comayagua');
          });
          break;
        case 'Copan':
          this.hondurasRegionService.getMunicipality4().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('Copan');
          });
          break;
        case 'Cortes':
          this.hondurasRegionService.getMunicipality5().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('Cortes');
          });
          break;
        case 'Choluteca':
          this.hondurasRegionService.getMunicipality6().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('Choluteca');
          });
          break;
        case 'El Paraiso':
          this.hondurasRegionService.getMunicipality7().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('El Paraiso');
          });
          break;
        case 'Francisco Morazan':
          this.hondurasRegionService.getMunicipality8().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('Francisco Morazan');
          });
          break;
        case 'Gracias A Dios':
          this.hondurasRegionService.getMunicipality9().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('Gracias A Dios');
          });
          break;
        case 'Intibuca':
          this.hondurasRegionService.getMunicipality10().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('Intibuca');
          });
          break;
        case 'Islas de La Bahia':
          this.hondurasRegionService.getMunicipality11().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('Islas de La Bahia');
          });
          break;
        case 'La Paz':
          this.hondurasRegionService.getMunicipality12().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('La Paz');
          });
          break;
        case 'Lempira':
          this.hondurasRegionService.getMunicipality13().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('Lempira');
          });
          break;
        case 'Ocotepeque':
          this.hondurasRegionService.getMunicipality14().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('Ocotepeque');
          });
          break;
        case 'Olancho':
          this.hondurasRegionService.getMunicipality15().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('Olancho');
          });
          break;
        case 'Santa Barbara':
          this.hondurasRegionService.getMunicipality16().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('Santa Barbara');
          });
          break;
        case 'Valle':
          this.hondurasRegionService.getMunicipality17().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('Valle');
          });
          break;
        case 'Yoro':
          this.hondurasRegionService.getMunicipality18().subscribe(data => {
            this.hondurasSelectedMunicipalityData = data;
            this.loadMapMunicipality('Yoro');
          });
          break;
        case 'Lago de Yojoa':
          break;
        default:
          break;
      }
    }

    loadMapRegion(region: string) {
      const styleRegion = (region2: any) => {
        return {
          fillColor: this.getColor(region),
          weight: 2,
          opacity: 1,
          color: 'white',
          dashArray: '3',
          fillOpacity: 0.5
        };
      };
      if (this.selectedRegionsGeojson) {
        this.selectedRegionsGeojson.remove();
      }
      this.selectedRegionsGeojson =
      geoJSON(this.hondurasSelectedRegionData, {style: styleRegion(region), onEachFeature: this.onEachFeatureRegionFuntion});
      this.layers.push(this.selectedRegionsGeojson);
      this.changeDetector.detectChanges();
    }

    loadMapMunicipality(region: string) {
      const styleRegion = (region2: any) => {
        return {
          fillColor: this.getColorDeparments(region),
          weight: 2,
          opacity: 1,
          color: 'white',
          dashArray: '3',
          fillOpacity: 0.5
        };
      };
      if (this.selectedMunicipalityGeojson) {
        this.selectedMunicipalityGeojson.remove();
      }
      this.selectedMunicipalityGeojson =
        geoJSON(this.hondurasSelectedMunicipalityData, {style: styleRegion(region), onEachFeature: this.onEachMunicipalityDeparmentFuntion});
      this.layers.push(this.selectedMunicipalityGeojson);
      this.changeDetector.detectChanges();
    }



    loadProject() {
      const filterCapitalizableGoodFuntion = (geoJsonFeature)  => {
        return geoJsonFeature.properties.CATEGORIA_INVERSION === '1';
      };

      const filterGoodFuntion = (geoJsonFeature)  => {
        return geoJsonFeature.properties.CATEGORIA_INVERSION === '2';
      };

      const filterWorkFuntion = (geoJsonFeature)  => {
        return geoJsonFeature.properties.CATEGORIA_INVERSION === '3';
      };

      const filterOtherFuntion = (geoJsonFeature)  => {
        return geoJsonFeature.properties.CATEGORIA_INVERSION === '4';
      };

      const filterAdministrativeExpenseFuntion = (geoJsonFeature)  => {
        return geoJsonFeature.properties.CATEGORIA_INVERSION === '5';
      };

      const filterServiceFuntion = (geoJsonFeature)  => {
        return geoJsonFeature.properties.CATEGORIA_INVERSION === '6';
      };

      const filterMaterialFuntion = (geoJsonFeature)  => {
        return geoJsonFeature.properties.CATEGORIA_INVERSION === '7';
      };

      const filterPersonalFuntion = (geoJsonFeature)  => {
        return geoJsonFeature.properties.CATEGORIA_INVERSION === '8';
      };

      const filterNoPersonalFuntion = (geoJsonFeature)  => {
        return geoJsonFeature.properties.CATEGORIA_INVERSION === '9';
      };


      const pointToLayerFuntion = (feature, latlng) => {
        if (feature.properties && feature.properties.CATEGORIA_INVERSION) {
          switch (feature.properties.CATEGORIA_INVERSION) {
            case '1':
              return L.marker(latlng, {icon: this.capitalizableGoodIcon});
              break;
            case '2':
              return L.marker(latlng, {icon: this.goodIcon});
              break;
            case '3':
              return L.marker(latlng, {icon: this.workIcon});
              break;
            case '4':
              return L.marker(latlng, {icon: this.otherIcon});
              break;
            case '5':
              return L.marker(latlng, {icon: this.administrativeExpenseIcon});
              break;
            case '6':
              return L.marker(latlng, {icon: this.serviceIcon});
              break;
            case '7':
              return L.marker(latlng, {icon: this.materialIcon});
              break;
            case '8':
              return L.marker(latlng, {icon: this.personalServiceIcon});
              break;
            case '9':
              return L.marker(latlng, {icon: this.noPersonalServiceIcon});
              break;
            default:
              return L.marker(latlng, {icon: this.capitalizableGoodIcon});
              break;
          }
        }
      };

      const whenClicked = (e) => {
        this.getProjectInformation(e);
      };

      const onEachFeature = (feature, layer) => {
        layer.on({
          click: whenClicked
        });
        layer.bindPopup('', {maxWidth: 500});
      };

      if (this.selectedCapitalizableGoodGeojson) {
        this.selectedCapitalizableGoodGeojson.remove();
      }

      if (this.selectedGoodGeojson) {
        this.selectedGoodGeojson.remove();
      }

      if (this.selectedWorkGeojson) {
        this.selectedWorkGeojson.remove();
      }

      if (this.selectedOtherGeojson) {
        this.selectedOtherGeojson.remove();
      }

      if (this.selectedAdministrativeExpenseGeojson) {
        this.selectedAdministrativeExpenseGeojson.remove();
      }

      if (this.selectedServiceGeojson) {
        this.selectedServiceGeojson.remove();
      }

      if (this.selectedMaterialGeojson) {
        this.selectedMaterialGeojson.remove();
      }

      if (this.selectedPersonalGeojson) {
        this.selectedPersonalGeojson.remove();
      }

      if (this.selectedNoPersonalGeojson) {
        this.selectedNoPersonalGeojson.remove();
      }

      this.selectedCapitalizableGoodGeojson =
        geoJSON(this.hondurasSelecteProjectData, {
          filter: filterCapitalizableGoodFuntion,
          pointToLayer: pointToLayerFuntion,
          onEachFeature: onEachFeature
        });


      this.selectedGoodGeojson =
        geoJSON(this.hondurasSelecteProjectData, {
          filter: filterGoodFuntion,
          pointToLayer: pointToLayerFuntion,
          onEachFeature: onEachFeature
        });

      this.selectedWorkGeojson =
        geoJSON(this.hondurasSelecteProjectData, {
          filter: filterWorkFuntion,
          pointToLayer: pointToLayerFuntion,
          onEachFeature: onEachFeature
        });

      this.selectedOtherGeojson =
        geoJSON(this.hondurasSelecteProjectData, {
          filter: filterOtherFuntion,
          pointToLayer: pointToLayerFuntion,
          onEachFeature: onEachFeature
        });

      this.selectedAdministrativeExpenseGeojson =
        geoJSON(this.hondurasSelecteProjectData, {
          filter: filterAdministrativeExpenseFuntion,
          pointToLayer: pointToLayerFuntion,
          onEachFeature: onEachFeature
        });

      this.selectedServiceGeojson =
        geoJSON(this.hondurasSelecteProjectData, {
          filter: filterServiceFuntion,
          pointToLayer: pointToLayerFuntion,
          onEachFeature: onEachFeature
        });

      this.selectedMaterialGeojson =
        geoJSON(this.hondurasSelecteProjectData, {
          filter: filterMaterialFuntion,
          pointToLayer: pointToLayerFuntion,
          onEachFeature: onEachFeature
        });

      this.selectedPersonalGeojson =
        geoJSON(this.hondurasSelecteProjectData, {
          filter: filterPersonalFuntion,
          pointToLayer: pointToLayerFuntion,
          onEachFeature: onEachFeature
        });

      this.selectedNoPersonalGeojson =
        geoJSON(this.hondurasSelecteProjectData, {
          filter: filterNoPersonalFuntion,
          pointToLayer: pointToLayerFuntion,
          onEachFeature: onEachFeature
        });


      const investmentCategoryListSelected = this.convertChecktoListSelected('CATEGORIA INVERSIÓN');

      if (this.selectedCapitalizableGoodGeojson && investmentCategoryListSelected.indexOf('1') > -1) {
        this.layers.push(this.selectedCapitalizableGoodGeojson);
      }

      if (this.selectedGoodGeojson && investmentCategoryListSelected.indexOf('2') > -1) {
        this.layers.push(this.selectedGoodGeojson);
      }

      if (this.selectedWorkGeojson && investmentCategoryListSelected.indexOf('3') > -1) {
        this.layers.push(this.selectedWorkGeojson);
      }

      if (this.selectedOtherGeojson && investmentCategoryListSelected.indexOf('4') > -1) {
        this.layers.push(this.selectedOtherGeojson);
      }

      if (this.selectedAdministrativeExpenseGeojson && investmentCategoryListSelected.indexOf('5') > -1) {
        this.layers.push(this.selectedAdministrativeExpenseGeojson);
      }

      if (this.selectedServiceGeojson && investmentCategoryListSelected.indexOf('6') > -1) {
        this.layers.push(this.selectedServiceGeojson);
      }

      if (this.selectedMaterialGeojson && investmentCategoryListSelected.indexOf('7') > -1) {
        this.layers.push(this.selectedMaterialGeojson);
      }

      if (this.selectedPersonalGeojson && investmentCategoryListSelected.indexOf('8') > -1) {
        this.layers.push(this.selectedPersonalGeojson);
      }

      if (this.selectedNoPersonalGeojson && investmentCategoryListSelected.indexOf('9') > -1) {
        this.layers.push(this.selectedNoPersonalGeojson);
      }

      this.changeDetector.detectChanges();
    }

    getCatalogFile() {
      this.progressRef.start();
      this.fileService.getCatalogFile().subscribe(res => {
        if (res) {
          const fileDocument =  this.fileService.dataURItoBlob('data:image/jpeg;base64,' + res);
          const fileOfBlob = new File([fileDocument], `catalogos.xlsx`,
            { type:  'xml', lastModified: Date.now() });
          FileSaver.saveAs(fileOfBlob);
          this.progressRef.complete();
        } else {
          swal('Archivo', ' No se puede descargar el archivo!', 'error');
          this.progressRef.complete();
        }
      });
    }

    getProjectFile() {
      this.progressRef.start();

      const sectorListSelected = this.convertChecktoListSelected('SECTOR');
      const institutionListSelected = this.convertChecktoListSelected('INSTITUCIÓN');
      const specificSourceListSelected = this.convertChecktoListSelected('FUENTE ESPECÍFICA');
      const cabinetListSelected = this.convertChecktoListSelected('GABÍNETE');

      this.fileService.searchByFilterProjectFile(cabinetListSelected.join(), institutionListSelected.join(),
        sectorListSelected.join(),  specificSourceListSelected.join()).subscribe(res => {
        if (res) {
          const fileDocument =  this.fileService.dataURItoBlob('data:image/jpeg;base64,' + res);
          const fileOfBlob = new File([fileDocument], `proyectos.xlsx`,
            { type:  'xml', lastModified: Date.now() });
          FileSaver.saveAs(fileOfBlob);
          this.progressRef.complete();
        } else {
          swal('Archivo', ' No se puede descargar el archivo!', 'error');
          this.progressRef.complete();
        }
      });
    }

  getCatalogAndProjectFile() {
    this.progressRef.start();

    const sectorListSelected = this.convertChecktoListSelected('SECTOR');
    const institutionListSelected = this.convertChecktoListSelected('INSTITUCIÓN');
    const specificSourceListSelected = this.convertChecktoListSelected('FUENTE ESPECÍFICA');
    const cabinetListSelected = this.convertChecktoListSelected('GABÍNETE');

    this.fileService.searchByFilterCatalogAndProjectFile(cabinetListSelected.join(), institutionListSelected.join(),
      sectorListSelected.join(),  specificSourceListSelected.join()).subscribe(res => {
      if (res) {
        const fileDocument =  this.fileService.dataURItoBlob('data:image/jpeg;base64,' + res);
        const fileOfBlob = new File([fileDocument], `Catalogos&proyectos.xlsx`,
          { type:  'xml', lastModified: Date.now() });
        FileSaver.saveAs(fileOfBlob);
        this.progressRef.complete();
      } else {
        swal('Archivo', ' No se puede descargar el archivo!', 'error');
        this.progressRef.complete();
      }
    });
  }
  }
