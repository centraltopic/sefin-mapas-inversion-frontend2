import {Model} from '../../core/models/model';

export interface Sector extends Model {
  nombre?: string;
}
