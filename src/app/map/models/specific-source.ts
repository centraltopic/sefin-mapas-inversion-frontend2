import {Model} from '../../core/models/model';

export interface SpecificSource extends Model {
  nombre?: string;
}
