import {Model} from '../../core/models/model';

export interface Institution extends Model {
  nombre?: string;
}
