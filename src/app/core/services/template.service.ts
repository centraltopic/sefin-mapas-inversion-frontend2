import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {BackendObject, Model} from '../models/model';
import swal from 'sweetalert2';
import {catchError} from 'rxjs/internal/operators';
import {environment} from '../../../environments/environment';

export const ADD = {title: 'Creación', text: 'Registro Creado Satisfactoriamente'};
export const UPDATE = {title: 'Actualización', text: 'Registro Actualizado Satisfactoriamente'};
export const DELETE = {title: 'Eliminación', text: 'Registro Eliminado Satisfactoriamente'};

export class TemplateService {

  constructor(
    protected readonly http: HttpClient,
    protected readonly url: string
  ) { }


  get(options?): Observable<any> {
    return this.http.get(`${environment.baseUrl}/${this.url}`, options)
      .pipe(
        catchError(this.handleError('get'))
      );
  }

  getOfURL(url, options?): Observable<any> {
    return this.http.get(`${environment.baseUrl}/${url}`, options)
      .pipe(
        catchError(this.handleError('get'))
      );
  }

  getMetaData(options?, manual?: boolean): Observable<any> {
    return this.http.get(`${environment.baseUrl}/${this.url}?metadata=1`, options)
      .pipe(
        catchError(this.handleError('getMetaData'))
      );
  }

  searchById(id: number): Observable<BackendObject> {
    if (id === null  || id === undefined) {
      return of(null);
    }

    return  this.http.get<BackendObject>(`${environment.baseUrl}/${this.url}/?id=${id}`).pipe(
      tap(_ => console.log(`searchByCode matching "${id}"`))
    );
  }

  post(body?, options?): Observable<any>  {
    return this.http.post(`${environment.baseUrl}/${this.url}`, body, options)
      .pipe(
        catchError(this.handleError('post'))
      );
  }

  put(body?, options?): Observable<any>  {
    return this.http.put(`${environment.baseUrl}/${this.url}`, body, options)
      .pipe(
        catchError(this.handleError('post'))
      );
  }

  putOfURL(url, body?, options?): Observable<any>  {
    return this.http.put(`${environment.baseUrl}/${url}`, body, options)
      .pipe(
        catchError(this.handleError('put'))
      );
  }


  getList(): Observable<any> {
    return this.http.get<BackendObject[]>(`${environment.baseUrl}/${this.url}`)
      .pipe(
        tap(data => {
          console.log(`getList success ${data.rows.length} rows`);
        })
      );
  }

  add(model: Model): Observable<any> {
    return this.http.post<Model>(`${environment.baseUrl}/${this.url}`, model).pipe(
      tap(resp => this.log(resp, ADD)),
      catchError(this.handleError<Model>('add'))
    );
  }

  update(model: Model): Observable<any> {
    return this.http.put<Model>(`${environment.baseUrl}/${this.url}`, model).pipe(
      tap(resp => this.log(resp, UPDATE)),
      catchError(this.handleError<Model>('update'))
    );
  }

  delete(model: Model | number): Observable<any> {
    const id = typeof model === 'number' ? model : model.id;
    const urlId = `${environment.baseUrl}/${this.url}/${id}`;
    return this.http.delete<Model>(urlId).pipe(
      tap(resp => this.log(resp, DELETE)),
      catchError(this.handleError<Model>('delete'))
    );
  }

  protected handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      console.error(`${operation} failed: ${error.message}`); // log to console instead
      this.logError();
      return of(result as T);
    };
  }

  protected log(resp, operation) {
    if (resp  && resp.result && resp.result.type) {
      if (resp.result.type === 'success') {
        swal({
          type: resp.result.type,
          title: operation.title,
          text: operation.text
        });
      } else {
        swal({
          type: resp.result.type,
          title: resp.result.title,
          text: resp.result.text
        });
      }
    } else if (resp  && resp.message && resp.message.severity) {
      if (resp.message.severity === 'success') {
        swal({
          type: resp.message.severity,
          title: resp.message.summary,
          text: operation.text
        });
      } else {
        swal({
          type: resp.message.severity,
          title: resp.message.summary,
          text: resp.message.detail
        });
      }
    }
  }

  protected logError() {
    swal({
      type: 'error',
      title: 'Error General',
      text: 'Ha Ocurrido un Error General por Favor Contacte con el Administrador'
    });
  }
}
